enum Priority: String {
    case high
    case middle
    case low
}
class FieldParam {
    let header: String
    let length: Int
    var placeholder: String?
    let code: Int?
    let priority: String
    
    init(header: String, length: Int, placeholder: String? = nil, code: Int? = nil, priority: Priority) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority.rawValue
    }
    
    func isFieldLength() -> Bool {
        length <= 25
    }
    
    func setFieldPlaceholder(with newPlaceholder: String) {
        self.placeholder = newPlaceholder
    }
    
}
let nameField = FieldParam(header: "Name", length: 25, placeholder: "Type your name", code: 1, priority: .high)
let surnameField = FieldParam(header: "Surname", length: 25, placeholder: "Type your name", code: 2, priority: .high)
let ageField = FieldParam(header: "Age", length: 3, code: 3, priority: Priority.middle)
let cityField = FieldParam(header: "City", length: 15, placeholder: "City", priority: .low)

struct FieldStructure {
    let header: String
    let length: Int
    var placeholder: String
    let code: Int
    
    
    init(header: String, length: Int, placeholder: String, code: Int) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
    }
    
    mutating func setFieldPlaceholder(with newPlaceholder: String) {
        self.placeholder = newPlaceholder
    }
    func isFieldLength() -> Bool {
        length <= 25
    }
    
}

let nameFieldStructure = FieldStructure(header: "Name", length: 25, placeholder: "Type your name", code: 1)
let surnameFieldStructure = FieldStructure(header: "Surname", length: 25, placeholder: "Type your name", code: 2)
