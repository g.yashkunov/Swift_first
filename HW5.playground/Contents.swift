import Foundation //Задание 1
class Shape {
    func calculateArea() -> Double {
        fatalError("not implemented")
    }
    
    func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }
    
    override func calculateArea() -> Double {
        height * width
    }
    
    override func calculatePerimeter() -> Double {
        2 * (height + width)
    }
}

class Circle: Shape {
    private let radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        Double.pi * pow(radius, 2)
    }
    
    override func calculatePerimeter() -> Double {
        2 * radius * Double.pi
    }
}



class Square: Shape {
    private let side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        side * side
    }
    
    override func calculatePerimeter() -> Double {
        4 * side
    }
}

let shapes: [Shape] = [Rectangle(height: 7.0, width: 12.0), Circle(radius: 20.0), Square(side: 44.0)]

var area: Double = 0
var perimeter: Double = 0

for shape in shapes {
    area += shape.calculateArea()
    perimeter += shape.calculatePerimeter()
}
print(area, perimeter)

// Задание 2
func findIndexWithGenerics<T: Equatable>(ofString valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}
let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
if let foundIndexString = findIndexWithGenerics(ofString: "лама", in: arrayOfString) {
    print("Индекс лама:\(foundIndexString)")
}
var element = 1.1
let arrayOfDouble = [element, 5.5]
if let foundIndexDouble = findIndexWithGenerics(ofString: 1.1, in: arrayOfDouble) {
    print("Индекс \(element): \(foundIndexDouble)")
}
element = 99
let arrayOfInt = [1, 5, 10, 15, 20, 50, 99, 1, 5, 10, 15, 20, 50, element]
if let foundIndexInt = findIndexWithGenerics(ofString: element, in: arrayOfInt) {
    print("Индекс \(element): \(foundIndexInt)")
}
