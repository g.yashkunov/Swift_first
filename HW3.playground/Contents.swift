import Foundation

//Задание 1.
//Пункт 1. Напишите функцию fibonacciNumber, в которой:
//а. Аргументом является целое число — параметр count: Int.
//b. Результатом является массив целых чисел.[Int]
func fibonacciNumber(count: Int) -> [Int] {
    if count > 1 {
        //c. Внутри объявлен массив storyPointsNumbers с типом Int
        var storyPointsNumbers = [1, 1]
        //d. Реализован цикл с алгоритмом Фибоначчи
        //e. Добавлено в массив storyPointsNumbers каждое число из последовательности
        var number1 = 1
        var number2 = 1
        for _ in 0 ..< count - 2 {
            let number = number1 + number2
            storyPointsNumbers.append(number)
            number1 = number2
            number2 = number
        }
        //f. Нужно вернуть массив storyPointsNumbers с результатами выполнения алгоритма
        return storyPointsNumbers
    } else {
        return []
        
    }
}
//Пункт 2. Вызовите функцию 2 раза с разными вводными данными (числа 5 и 7)
//Пункт 3. После каждого вызова функции fibonacciNumber распечатайте результат ее выполнения
var result = fibonacciNumber(count: 5)
print(result)
result = fibonacciNumber(count: 7)
print(result)

//Задание 2.
let printArrayFor: ([Int]) -> Void = { numbers in
    for i in 0...(numbers.count-1) {
        print(numbers[i])
    }
}
printArrayFor(result)


