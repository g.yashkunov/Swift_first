import Foundation
// Задача 1
var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)
// Задача 2
var milkPrice = 3.0
// Задача 3
milkPrice = 4.20
// Задача 4
var milkBottleCount: Int? = nil
milkBottleCount = 20
var profit: Double = 0.0
profit = milkPrice * Double(milkBottleCount!)
// Дополнительное задание*:
if let milkBottleCount = milkBottleCount {
    profit = milkPrice * Double(milkBottleCount)
} else {
    print("milkBottleCount не содержит значения")
}
//Принудительное развертывание может привести к ошибке если там будет nil
//Дополнительный пример
//let one: Int? = nil
//one!
// Задача 5
var employeesList = [String]()
employeesList.append(contentsOf: ["Иван", "Петр", "Геннадий", "Андрей", "Марфа"])

// Задача 6

var isEveryoneWorkHard = false
var workingHours = 40
workingHours = 10
if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}
print(isEveryoneWorkHard)
